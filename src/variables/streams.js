import React, { useState, useEffect } from "react";

export default function StreamsData() {
  const [data, setdata] = useState([]);
  useEffect(() => {
    fetch("http://127.0.0.1:5000/getStreams").then((res) =>
      res.json().then((data) => {
        // Setting a data from api
        console.log(data);
        setdata({
          Text: data.data,
        });
      })
    );
  }, []);

  return (
    <>
      <h2>React Fetch API Example</h2>
      <ul>
        {data.map((item, i) => {
          return <li key={i}>{item.Data}</li>;
        })}
      </ul>
    </>
  );
}

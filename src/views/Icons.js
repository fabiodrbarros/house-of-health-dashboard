/*!

=========================================================
* Black Dashboard React v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// reactstrap components
import { Card, CardHeader, CardBody, Row, Col, Button } from "reactstrap";

function Icons() {
  const history = useHistory();

  const redirectMe = (url, stream_id) => {
    history.push({ pathname: url, state: stream_id });
  };

  // TEXT DATA
  const [streams, setStreams] = useState([]);

  useEffect(() => {
    fetch("http://localhost:4998/getStreams")
      .then((res) => res.json())
      .then((data) => {
        setStreams(data);
      });
  }, []);

  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader></CardHeader>
              <CardBody className="all-icons">
                <Row>
                  {streams.map((streams, idx) => (
                    <Col
                      className="font-icon-list col-xs-6 col-xs-6"
                      lg="2"
                      md="3"
                      sm="4"
                      key={idx}
                    >
                      <div className="font-icon-detail">
                        <h5>{streams._id}</h5>
                        {streams._date}
                        <p></p>
                        <Button
                          color="primary"
                          onClick={() =>
                            redirectMe("/admin/notifications", streams._id)
                          }
                        >
                          View Stream
                        </Button>
                      </div>
                    </Col>
                  ))}
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}

export default Icons;

/*!

=========================================================
* Black Dashboard React v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from "react";

// reactstrap components
import {
  UncontrolledAlert,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";

function UserProfile() {
  const [moods, setMoods] = useState([]);

  useEffect(() => {
    fetch("http://localhost:4998/get_user_moods/62583b2f9487902947c6c547")
      .then((res) => res.json())
      .then((data) => {
        setMoods(data);
      });
  });

  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card className="card-user">
              <CardBody>
                <CardText />
                <div className="author">
                  <div className="block block-one" />
                  <div className="block block-two" />
                  <div className="block block-three" />
                  <div className="block block-four" />
                  <a href="#pablo" onClick={(e) => e.preventDefault()}>
                    <img
                      alt="..."
                      className="avatar"
                      src={require("assets/img/emilyz.jpg").default}
                    />
                    <h5 className="title">USER</h5>
                  </a>
                  <p className="description">OBS</p>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            {moods.map((t, idx) => (
              <UncontrolledAlert className="alert-with-icon" color="info">
                <span className="tim-icons icon-bell-55" data-notify="icon" />

                <span data-notify="message" key={idx}>
                  <b>{t.Date}</b> : {t.Value}
                </span>
              </UncontrolledAlert>
            ))}
          </Col>
        </Row>
      </div>
    </>
  );
}

export default UserProfile;
